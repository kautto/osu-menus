<h1>Building Usable Menus</h1>

<p>
  In surveys, one of our lowest scores was in findability of information.
  One of the easiest things you can do to improve this is to follow a few
  simple site navigation best practices.
</p>

<ul>
  <li><a href="#help-no-external">No external links.</a></li>
  <li><a href="#help-no-duplicates">No duplicate links.</a></li>
  <li><a href="#help-choices">Provide an appropriate number of choices.</a></li>
</ul>

<h2 id="help-no-external">No External Links (in the menu)</h2>

<h3>Key Problems</h3>
<h4>It confuses users.</h4>
<p>
  Unless otherwise identified, users expect the navigation for your site to be navigation for <strong>your site</strong>.
  They get confused when they click on a navigation link and find themselve transported to a totally different site. Confusing users is bad.
</p>

<h4>It adds mental strain.</h4>
<p>
  Even if the other site is "within your family", it will generally have a totally different site navigation.
  Users build mental models of a site's navigation, and a hierarchical model is one of the easiest to navigate.
  Using many external links changes the nature of your site's navigation from a hierarchical
  model to a web of links (like wikipedia). "Web" navigation models are notoriously hard on users for typical
  use cases.
</p>
<p>
  Those of you with a background in programming may recognize that mixing in external links in site navigation is the editorial equivalent of using the <a href="https://www.cs.utexas.edu/users/EWD/ewd02xx/EWD215.PDF">goto statement</a>.
</p>

<h4>Excessive use dilutes your message.</h4>
<p>
  You have a message you want to get out. Otherwise you wouldn't have a website.
  People who come to your website are there to get <strong>your message</strong>, and
  the menu is the system they use to find it. Every external link you add to your menu makes your
  message harder to find.
  Modern search engines are remarkably good, and people have many ways of arriving at someone else's site.
  Prioritizing another site's content at an equal level to your own is a disservice to
  people who are there to find your content.
</p>
<p>
  Walking downtown, you might ask a hot dog vendor for directions to the library,
  but you wouldn't expect him to put those directions on his menu. A particularly
  clever street vendor might even capture the tourist trade with a big map on
  the side of his truck, but when it comes to his menu, everything there will
  be something he sells.
</p>

<h3>Solutions</h3>
<p>
  Although links to external resources don't generally belong in your site's navigation, they are obviously very important. We recommend these approaches.
</p>

<h4>Introduce links in content</h4>
<p>Introducing external links in content is a great approach.</p>
<ul>
  <li>It prioritizes your message over the external resource.</li>
  <li>The introduction allows you to contextualize the external resource.</li>
  <li>Most designs visually differentiate external links in content.</li>
  <li>
    Users expect that links in your content could go anywhere so...
    <ul>
      <li>no extra confusion.</li>
      <li>no additional mental strain.</li>
    </ul>
  </li>
</ul>

<h4>Create a separate space for external resources.</h4>
<p>
  One of the key problems with external links in site navigation is
  not that the external links are in navigation, but that they are
  mixed in navigation with non-external links. If you separate
  the external links, you significantly reduce problems. Here are
  a few suggestions.
</p>
<ul>
  <li>Place external links in a page sidebar labelled "External Resources".</li>
  <li>Create reUsable landing page widgets of "External Links".</li>
  <li>Create a page that is an index of many external resources.</li>
</ul>

<h3>External References</h3>
<ul>
  <li>
    <a href="http://www.uxbooth.com/articles/better-user-orientation-through-navigation/">Better User Orientation through Navigation</a> by UxBooth.com
  </li>
  <li>
    <a href="http://webstyleguide.com/wsg3/3-information-architecture/3-site-structure.html">Web Style Guide on Navigation, Hierarchies, and Webs of Links</a> by WebStyleGuide.com
  </li>
</ul>

<h2 id="help-no-duplicates">No Duplicate Links (in the menu)</h2>
<p>
  It is often hard to organize your content into a strict hierarchy. An editor
  with both "Current" and "Future" student sections may be tempted to create a menu
  link in each section pointing to the same page. Resist the temptation.
</p>

<h3>Key Problems</h3>

<h4>Users don't know they are the same.</h4>
<p>
  You know the content is the same, but the user doesn't.
  Duplicating a menu item creates an additional, false, path
  for your visitors to explore.

  While links in content appear differently depending on whether
  or not a link has been visited, links in menus don't becuase
  such styling would conflict with visual cues designed to say,
  "you are here."
</p>

<h4>Users get lost.</h4>
<p>
  If you navigate to a page, that page will always appear to be in a particular section of
  a site. Adding links to that page in multiple sections won't change that.
</p>
<p>
  When a user clicks on a menu link within a section of the site, they expect to stay in that section.
  Taking them to a different section of the site is almost as jaring as taking them to an external site.
</p>

<h4>Your ideal message is probably different.</h4>
<p>
  Not always, but usually, the message you have about a particular topic is different
  in different sections of your site. This tends to be especially true if a site's
  navigation is organized by audience (undergraduates) and is probably less true of sites
  organized by product (degree program).
</p>

<p>What does a prospective student want to know about your bachelors program?</p>
<ul>
  <li>How does your program compare to your competition?</li>
  <li>Are there other flavors I may want to consider (BS/MS)</li>
  <li>How do I apply?</li>
  <li>How much does it cost?</li>
  <li>Is there a brochure?</li>
  <li>What are the career prospects?</li>
  <li>What is the curriculum like (for incoming students)?</li>
</ul>

<p>What does a current student want to know about a bachelor's program?</p>
<ul>
  <li>How do I schedule an appointment with my advisor?</li>
  <li>How do I go from a pre-major to being in the major?</li>
  <li>How far along am I in the curricular requirements?</li>
  <li>What are the curriculum requirements for me?</li>
</ul>

<h3>Solutions</h3>
<p>
  There are many different ways of resolving duplicate links.
  Which approach to use depends on the underlying needs.
</p>

<h4>Customize the message</h4>
<p>
  If you re-evaluate what your audience wants to see in each section,
  you may find that you have fairly different things you need to convey.
  This could result in creating distinct content.
  It's a good idea to explore this option when organizing your site
  by audience.
</p>

<h4>Create links in content</h4>
<p>
  In many cases, the easiest solution is to create a link to a page in
  another section within your content just like you would an external link.
  This allows you to contextualize the information. This is a good approach
  when the information is of secondary importance.
</p>
<p>
  For example, if you have a prospective student page about a bachelors
  program, you might make a sidebar block of "current student resources"
  to include references to detailed curriculum requirements, advising information, etc.
  This prevents restating the information (which is bad for search engine optimization)
  and allows you to convey that the information targets a different type of user.
</p>

<h4>Reorganize</h4>
<p>
  If you find you are regularly struggling with duplicate menu entries,
  you may need to look at the overall organization of your site. Card sorting
  is a great way to re-evaluate how to organize your content.
</p>
<p>
  To try out card sorting, write the title (or keywords) of every page on an
  index card. Then bring in some of your target audience members and have
  them arrange the cards into hierarchies. Doing this excercise with a
  handful of users will help reveal how your users expect your navigation
  to work and may yield additional insights into deduplicating links.
</p>

<h3>External Resources</h3>
<ul>
  <li><a href="http://www.nngroup.com/articles/reduce-redundancydecrease-duplicated-design-decisions/">Reduce Redundancy: Decrease Duplicated Design Decisions</a> by the Nielsen Norman Group</li>
  <li><a href="http://sixrevisions.com/usabilityaccessibility/card-sorting/">Usability Testing with Card Sorting</a> by Six Revisions</li>
</ul>

<h2 id="help-choices">Provide an Appropriate Number of Choices</h2>
<p>
  Many editors create too many or too few options in a given section, or sub-section of the menu.
  The right number of choices depends greatly on how those choices are presented and how familiar
  the user is with the site or its content.
</p>

<h3>Key Problems</h3>

<h4>Users can't handle many undifferentiated choices.</h4>
<p>If you present someone with 30 undifferentiated options, it's too hard for them to find the thing they need unless they know <strong>exactly</strong> what they are looking for. Ten undifferentiated choices is around a reasonable default upper limit in <strong>most</strong> situations.</p>

<h4>Sections with too few items hide information.</h4>
<p>
  There is a clear usability tradeoff between depth and findability.
  Users have to drill down to find each new layer in the menu.
  Sites that go to new depths for just 1-2 links frustrate users.
</p>

<h3>Solutions</h3>
<h4>Create sections</h4>
<p>If you have too many choices, reorganize your content into multiple subsections.</p>

<h4>Merge pages</h4>
<p>If you have too choices, consider merging your 1 or 2 pages into their parent page.</p>

<h3>External Resources</h3>
<ul>
  <li><a href="http://www.nngroup.com/articles/flat-vs-deep-hierarchy/">Flat vs Deep Website Hierarchies</a> by the Nielsen Norman Group.</li>
</ul>
