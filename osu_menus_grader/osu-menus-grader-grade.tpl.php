<?php
/**
 * @file
 *  Grade theme function.
 */

/*

  $grade = array(
    'items' => array(
      'earned' => 0,
      'max' => 70,
    ),
    'organization' => array(
      'earned' => 0,
      'max' => 30,
    ),
    'total' => array(
      'earned' => 0,
      'max' => 100,
      'letter' => '',
    ),
  );

  $metrics = array(

  	// Link metrics.
    'links' => count(array_keys($tree)),
    'duplicate_links' => 0,
    'external_links' => 0,

    // Organization metrics.
    'parents' => 0,
    'max_children_violations' => 0,
    'min_children_violations' => 0,
  );

*/


?>
<div id="osu-menus-grade" class="<?php print $grade['total']['status'] ?>">
  <p>Your menu grade is:</p>
  <div class="osu-menus-grade-letter"><?php print $grade['total']['letter'] ?></div>
  <div class="osu-menus-grade-points"><?php print $grade['total']['earned'] ?> out of 100</div>
  </p>

  <?php if ($grade['total']['earned'] < 100): ?>

  <p>Potential Problems:</p>
  <ul>

    <?php if ($metrics['external_links']): ?>
    <li><?php print $metrics['external_links'] ?> of <?php print $metrics['links'] ?> links are external.</li>
    <?php endif; ?>

    <?php if ($metrics['duplicate_links']): ?>
    <li><?php print $metrics['duplicate_links'] ?> of <?php print $metrics['links'] ?> links are duplicates.</li>
    <?php endif; ?>

    <?php if ($metrics['max_children_violations']): ?>
    <li><?php print $metrics['max_children_violations'] ?> of <?php print $metrics['parents'] ?> branches have more than 10 items.</li>
    <?php endif; ?>

    <?php if ($metrics['min_children_violations']): ?>
    <li><?php print $metrics['min_children_violations'] ?> of <?php print $metrics['parents'] ?> branches have fewer than 2 items.</li>
    <?php endif; ?>

  </ul>
  <p>Improve your score by following <?php print l('best practices', 'help/osu_menus_grader/best-menu-usability') ?>.
  <?php endif; ?>
</div>
