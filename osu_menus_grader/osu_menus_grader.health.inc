<?php
/**
 * @file
 * Adds health module integration.
 */

/**
 * Implements hook_health_monitors().
 *
 * @return array
 */
function osu_menus_grader_health_monitors() {
  $monitors = array();
  $monitors['grade'] = array(
    'name' => l('Menu Grader', 'admin/menu'),
    'group' => t('Navigation'),
    'description' => t('Evaluates site navigation checking for several common issues.'),
    'args' => array('okay' => OSU_MENUS_GRADER_OKAY, 'warning' => OSU_MENUS_GRADER_WARNING),
  );
  return $monitors;
}

/**
 * Implements hook_health_monitor_grade().
 *
 * @param $args
 * @return array
 */
function osu_menus_grader_health_monitor_grade($args){

  $grade = osu_menus_grader_grade('main-menu');

  $options = array('!earned' => $grade['total']['earned'], '!letter' => $grade['total']['letter']);
  if ($grade['total']['earned'] < $args['warning']) {
    $status = HEALTH_ERROR;
    $message = t('Menu grade is less than 70%', $options);
  }
  else if ($grade['total']['earned'] < $args['okay']) {
    $status = HEALTH_WARNING;
    $message = t('Menu grade is less than 83%', $options);
  }
  else {
    $status = HEALTH_OKAY;
    $message = t('Your menu is pretty good.');
  }

  $message .= '<br />' . t('Details: Grade is !letter (!earned%).', $options);
  return health_monitor_status($status, $message);
}
