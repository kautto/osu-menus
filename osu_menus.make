api = 2
core = 7.x

; Superfish module
projects[superfish][version] = 1.x 
projects[superfish][subdir] = contrib

; This patch modifies the superfish make file to download the correct library version.
; See https://drupal.org/node/2163545
; See https://code.osu.edu/openosu/osu_menus/issues/8
projects[superfish][patch][2163545] = https://drupal.org/files/issues/issue-2163545.patch

; Superfish library - included via superfish module make file
; libraries[superfish][download][type] = get
; libraries[superfish][download][url] = https://github.com/mehrpadin/Superfish-for-Drupal/archive/1.x.zip
; libraries[superfish][directory_name] = superfish
; https://drupal.org/files/issues/issue-2163545.patch

; Jquery easing - using engineering github account for now
libraries[easing][download][type] = get
libraries[easing][download][url] = https://raw.github.com/osu-eng/jquery.easing/master/jquery.easing.js
libraries[easing][directory_name] = easing
