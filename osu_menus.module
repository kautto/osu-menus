<?php
/**
 * @file
 * osu_menus.module
 *
 * @author OSU Engineering Web Services
 *
 * @brief Incorporates menu-related features and components for the Open OSU
 * distribution.
 */

/**
 * Implements hook_permission().
 */
function osu_menus_permission() {

  return array(
    'administer osu_menus' => array(
      'title' => t('Administer menus and menu items'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * We add a top level menu entry for the main menu here.
 * We also duplicate some of the menu admin interface.
 */
function osu_menus_menu() {

  $items = array();

  $items['admin/menu'] = array(
    'title' => 'Menu',
    'description' => 'Manage the organization of pages in your site.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('menu_overview_form', menu_load('main-menu')),
    'access arguments' => array('administer osu_menus'),
    'file path' => drupal_get_path('module', 'menu'),
    'file' => 'menu.admin.inc',
  );

  $items['admin/menu/add'] = array(
    'title' => 'Add link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'menu_edit_item',
      'add',
      NULL,
      menu_load('main-menu'),
    ),
    'access arguments' => array('administer osu_menus'),
    'type' => MENU_LOCAL_ACTION,
    'file path' => drupal_get_path('module', 'menu'),
    'file' => 'menu.admin.inc',
  );

  $items['admin/menu/item/%menu_link/delete'] = array(
    'title' => 'Delete menu link',
    'page callback' => 'menu_item_delete_page',
    'page arguments' => array(3),
    'access arguments' => array('administer osu_menus'),
    'file path' => drupal_get_path('module', 'menu'),
    'file' => 'menu.admin.inc',
  );

  $items['admin/menu/item/%menu_link/edit'] = array(
    'title' => 'Edit menu link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('menu_edit_item', 'edit', 3, NULL),
    'access arguments' => array('administer osu_menus'),
    'file path' => drupal_get_path('module', 'menu'),
    'file' => 'menu.admin.inc',
  );

  $items['admin/menu/item/%menu_link/reset'] = array(
    'title' => 'Reset menu link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('menu_reset_item_confirm', 3),
    'access arguments' => array('administer osu_menus'),
    'file path' => drupal_get_path('module', 'menu'),
    'file' => 'menu.admin.inc',
  );
  return $items;

}

/**
 * Implements of hook_form_alter().
 *
 * This makes it so people can access menus configured in forms.
 */
function osu_menus_form_alter(&$form, $form_state, $form_id) {

  if (isset($form['menu']['link']['parent']) && !user_access('administer menu')) {
    $form['menu']['#access'] = user_access('administer osu_menus');
  }
}

/**
 * Alters the menu overview form for our special paths.
 *
 * We use this to capture menu links for the main menu, route them to our interface.
 */
function osu_menus_form_menu_overview_form_alter(&$form, &$form_state, $form_id) {
  // Add validator to limit number of top level links.
  $form['#validate'][] = 'osu_menus_menu_overview_form_validate';

  // We only want to modify this form if we're at our special path.
  if (arg(1) != 'structure') {

    $external = array();
    $internal = array();
    $total = 0;

    // Iterate over all links.
    foreach (array_keys($form) as $key) {
      if (preg_match('/^mlid:[0-9]+$/', $key)) {

        $path = $form[$key]['#item']['link_path'];

        // Keep track of internal, external, and duplicates.
        if (preg_match('|^http://|', $path)) {
          $external[$path] = isset($external[$path]) ? $external[$path] + 1 : 1;
        }
        else {
          $internal[$path] = isset($internal[$path]) ? $internal[$path] + 1 : 1;
        }
        $total++;

        // Correct edit / delete menu item links.
        foreach (array_keys($form[$key]['operations']) as $op) {
          $form[$key]['operations'][$op] = preg_replace('|^admin/structure|', 'admin', $form[$key]['operations'][$op]);
        }
      }
    }

    // Correct empty text add link path.
    if (array_key_exists('#empty_text', $form)) {
      $form['#empty_text'] = t('There are no menu links yet. <a href="@link">Add link</a>.', array('@link' => url('admin/menu/add')));
    }

    // Add redirect to correct path.
    $form['#submit'][] = 'osu_menus_menu_redirect';

    if (module_exists('osu_menus_grader')) {
      drupal_add_css(drupal_get_path('module', 'osu_menus') . '/osu_menus.css', array(
        'group' => CSS_DEFAULT,
        'every_page' => FALSE,
      ));

      // Fetch metrics up front to save time later.
      $tree = menu_build_tree($form['#menu']['menu_name']);
      $options = array(
        'min_children' => 2,
        'max_children' => 12,
      );
      $metrics = osu_menus_grader_tree_metrics($tree, $options);

      // Convert metrics into a grade.
      $grade = osu_menus_grader_grade($form['#menu']['menu_name'], $metrics);

      // Add to form.
      $grade_html = theme('osu_menus_grader_grade', array(
        'grade' => $grade,
        'metrics' => $metrics
      ));
      $form['grade'] = array(
        '#markup' => $grade_html,
        '#weight' => -100,
      );
    }
    // here?
  }
}

/**
 * Custom validation callback to ensure menus are limited to 8 items.
 */
function osu_menus_menu_overview_form_validate($form, &$form_state) {
  $top_level_items = 0;

  if (count($form_state['values']) > 0) {
    foreach ($form_state['values'] as $key => $value) {
      if (substr($key, 0, 5) == 'mlid:') {
        // Menu link.
        if (intval($value['plid']) == 0) {
          $top_level_items++;
        }
      }
    }
  }

  $max_top_level_items = (int) variable_get('osu_menus_max_top_level_items', 8);
  if ($max_top_level_items > 0 && $top_level_items > $max_top_level_items) {
    // There are already N top-level items, this cannot be added as one.
    form_set_error('top_level_items', t('The menu can only have @max_items links as top level items.', array('@max_items' => $max_top_level_items)));
  }
}

/**
 * Define a custom menu item form to add a custom submit callback.
 */
function osu_menus_form_menu_edit_item_alter(&$form, &$form_state, $form_id) {
  $form['#validate'][] = 'osu_menus_form_menu_edit_item_validate';

  // We only want to modify this form if we're at our special path.
  if (arg(1) != 'structure') {
    $form['#submit'][] = 'osu_menus_menu_redirect';
  }
}

/**
 * Defines custom callback validation to limit number of top-level menu items.
 */

function osu_menus_form_menu_edit_item_validate($form, &$form_state) {
  $mlid = (int) $form_state['values']['mlid'];
  $split = explode(':', $form_state['values']['parent'], 2);
  $plid = (int) $split[1];
  if ($plid == 0) {
    // Top level item.
    $menu_main_menu = menu_main_menu();
    $main_menu_top_level_items = count($menu_main_menu);
    if (!isset($menu_main_menu['menu-' . $mlid])) {
      // This was previously not a top level item.
      $main_menu_top_level_items++;
    }
    $max_top_level_items = (int) variable_get('osu_menus_max_top_level_items', 8);
    if ($max_top_level_items > 0 && $main_menu_top_level_items > $max_top_level_items) {
      // There are already N top-level items, this cannot be added as one.
      form_set_error('top_level_items', t('The menu can only have @max_items links as top level items.', array('@max_items' => $max_top_level_items)));
    }
  }
}

/**
 * Define a custom menu item form to add a custom submit callback.
 */
function osu_menus_form_menu_item_delete_form_alter(&$form, &$form_state, $form_id) {

  // We only want to modify this form if we're at our special path.
  if (arg(1) != 'structure') {
    $form['actions']['cancel']['#href'] = 'admin/menu';
    $form['#submit'][] = 'osu_menus_menu_redirect';
  }
}

/**
 * Implements hook_form_node_form_alter().
 *
 * Add validator to content types that need extra validation for menu links.
 */
function osu_menus_form_node_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'osu_page_node_form':
    case 'osu_landing_page_node_form':
      // Add validator to these types.
      $form['#validate'][] = 'osu_menus_form_node_form_validate';
      break;
  }
}

/**
 * Implement callback function for validator from osu_menus_form_node_form_alter().
 */
function osu_menus_form_node_form_validate($form, &$form_state) {
  if ((bool) $form_state['values']['menu']['enabled']) {
    // Only check if menu link enabled for this node.
    $split = explode(':', $form_state['values']['menu']['parent'], 2);
    $plid = (int) $split[1];
    if ($plid == 0) {
      // Top level item.
      $main_menu_items = count(menu_main_menu());
      if (!osu_menus_is_top_level_link(intval($form_state['node']->menu['mlid']))) {
        // New top level item .
        $main_menu_items++;
      }
      $max_top_level_items = (int) variable_get('osu_menus_max_top_level_items', 8);
      if ($max_top_level_items > 0 && $main_menu_items > $max_top_level_items) {
        // There are already N top-level items, this cannot be added as one.
        form_set_error('top_level_items', t('The menu can only have @max_items links as top level items.', array('@max_items' => $max_top_level_items)));
      }
    }
  }
}

/**
 * Checks if the provided mlid is one of the current top-level menu links.
 */
function osu_menus_is_top_level_link($mlid) {
  $found = FALSE;
  $mlid = (int) $mlid;
  $menu_main_menu = menu_main_menu();
  foreach ($menu_main_menu as $key => $value) {
    $split = explode(' ', $key, 2);
    $split = explode('menu-', $split[0]);
    $key = (int) $split[1];
    if ($key == $mlid) {
      // Found matching mlid.
      $found = TRUE;
      break;
    }
  }
  return $found;
}


/**
 * Make sure we stay on the non-structure path.
 */
function osu_menus_menu_redirect($form, &$form_state) {

  // This isn't working, but drupal_goto seems to work
  // $form_state['redirect'] = 'admin/menu';
  drupal_goto('admin/menu');
}


/**
 * Implements hook_role_perm_set().
 */
function osu_menus_role_perm_set() {
  // Set of all permissions being handled.
  $permissions = array(
    'administer osu_menus',
    'administer menu',
  );

  $ce = array(
    'administer osu_menus',
  );

  $admin = array(
    'administer osu_menus',
    'administer menu',
  );
  $admin = array_merge($admin, $ce);

  // No access to pages.
  $no_access = array();

  return array(
    // Set of permissions that will be toggled on or off.
    'permissions' => $permissions,
    // Grant permissions to the set of permissions defined above, based on role.
    // If the permission is defined in 'permissions' and omitted from the
    // grants, the role will not be given that permission.
    'grants' => array(
      'site administrator' => $admin,
      'site manager' => $ce,
      'content editor' => $ce,
      'content author' => $no_access,
      'content contributor' => $no_access,
      'authenticated user' => $no_access,
      'anonymous user' => $no_access,
    ),

  );

}

/**
 * Implements hook_menu_block_blocks().
 *
 * Creates a menu block for second-level+ layered navigation.
 */
function osu_menus_menu_block_blocks() {
  return array(
    'osu_menus_active_submenu' => array(
      //'menu_name' => MENU_TREE__CURRENT_PAGE_MENU,
      'menu_name' => 'main-menu',
      'title_link' => FALSE,
      'admin_title' => 'Active Page Submenu',
      'level' => 2,
      'depth' => 3,
    ),
  );
}


/**
 * Uses menu_tree_build() as an alternative means to generating a page-specific
 * menu (intead of using menu_block).
 */
function osu_menus_render_active_submenu() {
  /*
   * Determine the current node ID to check the content type: this allows us to
   * default to specific menu parents by content type.
   */

  /*
    $parent_mlid = osu_menus_get_parent_mlid();
    if ($parent_mlid == 0) {
      $level = 2;
    }
    else {
      $level = 2;
    }

    $parent_mlid = 0;
  */

  osu_menus_set_active_item();
  osu_menus_set_breadcrumb();

  $config = array(
    'delta' => '',
    'menu_name' => 'main-menu',
    'parent_mlid' => 0,
    'title_link' => 1,
    'admin_title' => '',
    'level' => 2,
    'follow' => 0,
    'depth' => 0,
    'expanded' => 0,
    'sort' => 0,
  );

  $tree = menu_tree_build($config);
  return $tree;
}


/**
 * Set the active menu item based on the content type.
 */
function osu_menus_set_active_item() {
  if (arg(0) == 'node') {
    $path = osu_menus_content_type_base_path();
    if (!empty($path)) {
      osu_menus_set_path($path);
    }
  }
}

/**
 * This sets the path to something under /$path_root/.
 *
 * @param string $path_root
 *   path root
 */
function osu_menus_set_path($path_root) {
  $trail = menu_get_active_trail();
  if (count($trail) == 2) {
    if ($trail[1]['menu_name'] != 'main-menu') {
      menu_tree_set_path('main-menu', $path_root);
    }
  }
}


/**
 * Helper function that returns the base path for a content_type.
 *
 * e.g. for a 'news_article', the base path is 'news'
 *
 * @param string $type
 *   Content type. If nothing specified, use arg(1) to determine type.
 *
 * @returns string
 *   The base path.
 */
function osu_menus_content_type_base_path($type = '') {
  if (empty($type)) {
    if (arg(0) == 'node') {
      $nid = arg(1);
      $node = node_load($nid);
      $type = $node->type;
    }
  }

  $base_path = '';

  if (!empty($type)) {
    switch ($type) {
      case 'osu_news_article':
        $base_path = 'news';
        break;

      case 'osu_blog_post':
        $base_path = 'blog';
        break;

      case 'osu_event':
        $base_path = 'events';
        break;

      case 'osu_person':
        $base_path = 'directory';
        break;

      case 'osu_course_offering':
        $base_path = 'courses';
        break;

      case 'osu_student_orgs':
        $base_path = 'studentorgs';
        break;
    }
  }

  return $base_path;
}


/**
 * Sets the breadcrumb for nodes that might not otherwise get a proper path,
 * for example News Articles.
 *
 * Does not do anything for other content types.
 */
function osu_menus_set_breadcrumb() {
  $base_path = osu_menus_content_type_base_path();
  if (!empty($base_path)) {

    // Try to find the menu item matching the base path for this content type,
    // if one exists.
    $menu_links = menu_load_links('main-menu');
    $mlid = 0;
    foreach ($menu_links as $item) {
      if ($item['link_path'] == $base_path) {
        // Found it.
        $mlid = $item['mlid'];
        $plid = $item['plid'];
      }
    }

    if ($mlid > 0) {
      $breadcrumb = array();
      $menu_items = array();
      while ($mlid > 0) {
        $item = menu_link_load($mlid);
        $menu_items[$mlid] = $item;
        $mlid = $item['plid'];
      }
      $menu_items = array_reverse($menu_items, TRUE);

      $breadcrumb[] = l(t('Home'), '<front>');

      foreach ($menu_items as $item) {
        $breadcrumb[] = l(t($item['link_title']), $item['link_path']);
      }
      $breadcrumb[] = drupal_get_title();

      drupal_set_breadcrumb($breadcrumb);
    }
  }
}
